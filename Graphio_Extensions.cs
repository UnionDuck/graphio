﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphio
{
    internal static class Graphio_Extensions
    {
        public static bool ContainsOperator(this string str)
        {
            if (str.Contains("*") || str.Contains("/") || str.Contains("+") || str.Contains("-") || str.Contains("^"))
                return true;
            else return false;
        }

        public static bool IsGraphVariable(this char chr)
        {
            switch (chr)
            {
                case 'x':
                case 'X':
                case 'y':
                case 'Y':
                    return true;
                default: return false;
            }
        }

        public static Graphio_EquationVariable GetVariable(this char chr)
        {
            switch (chr)
            {
                case 'x':
                case 'X':
                    return Graphio_EquationVariable.X;
                case 'y':
                case 'Y':
                    return Graphio_EquationVariable.Y;
                default: throw new Exception("Not x or y"); //TODO: anything but this
            }
        }
        /// <summary>
        /// Check if this character is a number, variable, operation or parenthesis
        /// </summary>
        /// <param name="chr"></param>
        /// <returns></returns>
        public static Graphio_EquationPartType GetEquationPartType(this char chr)
        {
            if (chr.IsGraphVariable())
            {
                return Graphio_EquationPartType.Variable;
            }
            if(chr == '(' || chr == ')')
            {
                return Graphio_EquationPartType.Parenthesis;
            }
            if (new[] { '*', '/', '+', '-', '^', '%' }.Contains(chr))
            {
                return Graphio_EquationPartType.Operation;
            }
            if(chr == '=')
            {
                return Graphio_EquationPartType.Equals;
            }
            else return Graphio_EquationPartType.Number;
        }
        /// <summary>
        /// Get the operation that this character represents
        /// </summary>
        /// <param name="chr"></param>
        /// <returns></returns>
        public static Graphio_EquationOperation GetEquationOperation(this char chr)
        {
            return chr switch
            {
                '*' => Graphio_EquationOperation.Mult,
                '/' => Graphio_EquationOperation.Div,
                '+' => Graphio_EquationOperation.Add,
                '-' => Graphio_EquationOperation.Sub,
                '^' => Graphio_EquationOperation.Pow,
                '%' => Graphio_EquationOperation.Mod,
                _ => default,//TODO: throw error
            };
        }

        public static Graphio_ParenthesisType GetParenthesisType(this char chr)
        {
            return chr switch
            {
                '(' => Graphio_ParenthesisType.Open,
                ')' => Graphio_ParenthesisType.Close,
                _ => throw new Exception("Not parenthesis"),//TODO: not this
            };
        }

        public static string ToString(this Graphio_ParenthesisType parenth)
        {
            if (parenth == Graphio_ParenthesisType.Close)
                return ")";
            else return "(";
        }

        public static string ToString(this Graphio_EquationOperation op)
        {
            return op switch
            {
                Graphio_EquationOperation.Add => "+",
                Graphio_EquationOperation.Sub => "-",
                Graphio_EquationOperation.Mult => "*",
                Graphio_EquationOperation.Div => "/",
                Graphio_EquationOperation.Pow => "^",
                Graphio_EquationOperation.Mod => "%",
                Graphio_EquationOperation.RemoveParentheses => "",
                _ => "",
            };
        }
    }
}
