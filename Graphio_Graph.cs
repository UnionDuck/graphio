using MyBox;
using System;
using System.Runtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vectrosity;
using System.Text;
using UnityEngine.UI;
using System.Linq;

namespace Graphio
{
    [Serializable]
    public class Graphio_ValuesList
    {
        public List<Vector2> list;
        public Color colour;
    }

    [Serializable]
    internal class Graphio_FunctionsList
    {
        internal string function;
        internal Color colour;
    }

    [Serializable]
    public class StringCollection : CollectionWrapper<string> { }

    public class Graphio_Graph : MonoBehaviour
    {
        #region inspector fields

        [Header("Grahio Setup")]
        [Space]
        [SerializeField]
        private Graphio_GraphType Type;
        [SerializeField]
        private string XAxisName = "Independent Variable";
        [SerializeField]
        private string YAxisName = "Dependent Variable";
        [SerializeField]
        private int Width = 100;
        [SerializeField]
        private int Height = 100;
        [SerializeField]
        private bool BackgroundGrid = true;
        [SerializeField]
        [Tooltip("If grid lines aren't rendering try increasing the width")]
        private float GridLineWidth = 1f;
        [Separator]
        [Header("Pick one")]
        [SerializeField]
        [Tooltip("Will ignore Height and fit to Width")]
        private bool FitToWidth = false;
        [SerializeField]
        [Tooltip("Will ignore Width and fit to Height")]
        private bool FitToHeight = false;
        [SerializeField]
        [Tooltip("Will scale graph to fit with current Width and Height")]
        private bool FitToBoth = true;
        [Separator]
        [SerializeField]
        private float PipLength = 2f;
        [SerializeField]
        private float BarChartBarWidth = 50f;
        [SerializeField]
        private float BarChartBarSeperator = 50f;
        [SerializeField]
        [Tooltip("How far away from the actual line of the axis the label should sit")]
        private float LabelOffsetFromAxis = 15f;
        [SerializeField]
        [Tooltip("The gap around the graph")]
        private float EdgeOffset = 10f;
        [SerializeField]
        private bool RoundAxisValues = true;
        [ConditionalField(nameof(RoundAxisValues), false)]
        [SerializeField]
        private int RoundToNearest = 10;

        [SerializeField]
        private GameObject LabelTemplate;
        [SerializeField]
        private GameObject Background;
        [SerializeField]
        private GameObject Canvas;
        [SerializeField]
        private GameObject BarChartBar;

        [Separator]

        [SerializeField]
        private bool AutoGenerateXLabels;
        [Header("X Labels")]
        [Header("First and Last Values (Autogen) only used if graphing from equation")]
        [ConditionalField(nameof(AutoGenerateXLabels), true)]
        [SerializeField]
        private StringCollection XLabels;       
        [ConditionalField(nameof(AutoGenerateXLabels))]
        [SerializeField]
        private float FirstX;
        [ConditionalField(nameof(AutoGenerateXLabels))]
        [SerializeField]
        private float LastX;
        [ConditionalField(nameof(AutoGenerateXLabels))]
        [Tooltip("The step between X axis values (autogen)")]
        [SerializeField]
        private float StepX;
        [Tooltip("How many values between each text label")]
        [SerializeField]
        private float XAxisTextLabelFrequency = 5;

        [Space]

        [SerializeField]
        private bool AutoGenerateYLabels;
        [Header("Y Labels")]
        [Header("First and Last Values (Autogen) only used if graphing from equation")]
        [ConditionalField(nameof(AutoGenerateYLabels), true)]
        [SerializeField]
        private StringCollection YLabels;        
        [ConditionalField(nameof(AutoGenerateYLabels))]
        [SerializeField]
        private float FirstY;
        [ConditionalField(nameof(AutoGenerateYLabels))]
        [SerializeField]
        private float LastY;
        [ConditionalField(nameof(AutoGenerateYLabels))]
        [Tooltip("The step between y axis values (autogen)")]
        [SerializeField]
        private float StepY;
        [Tooltip("How many values between each text label")]
        [SerializeField]
        private int YAxisTextLabelFrequency = 5;

        [Space]
        [Header("Graph Data")]
        [Header("Use a new list for each line on line chart")]
        [Header("Use a new list for each bar on bar chart (use y values)")]
        [SerializeField]
        [Tooltip("You can add multiple lists of data points")]
        private List<Graphio_ValuesList> Data = new List<Graphio_ValuesList>();

        [Space]
        [Separator("Functions", true)]
        [ConditionalField(nameof(Type), false, Graphio_GraphType.Line)]
        [SerializeField]
        private bool GraphFromEquation = false;
        [ConditionalField(nameof(GraphFromEquation))]
        [SerializeField]
        [Tooltip("Increment between minimum and maximum value (taken from axis labels)")]
        private float ValueFrequency = 0.1f;
        [ConditionalField(nameof(GraphFromEquation))]
        [SerializeField]
        [Tooltip("Format x/y=[e.g. x/y+2*2]")]
        private List<Graphio_FunctionsList> Equations = new List<Graphio_FunctionsList>();
        [Separator]

        #endregion

        private Canvas cvsGraphCanvas;
        private RectTransform rctGraphTransform;
        /// <summary>
        /// Current value of x for f(x)
        /// </summary>
        private float XValue = 1;
        /// <summary>
        /// Current value of y for f(y)
        /// </summary>
        private float YValue = 1;
      
        private readonly float LabelCorrectionAmount = 8; //amount to adjust label left/doww on x/y axis by in order to centre it around pips

        private float Scale = 2;

        //if fitting to both width and height
        private float xScale = 1;
        private float yScale = 1;

        private readonly List<List<Vector2>> GraphValues = new List<List<Vector2>>();
        private readonly List<Graphio_Equation> Graphio_Equations = new List<Graphio_Equation>();

        private readonly List<VectorLine> currentLines = new List<VectorLine>();
        private readonly List<GameObject> currentGraphExtras = new List<GameObject>(); //list of objects such as labels that need to be removed if redrawing graph

        private readonly List<Color> lineColours = new List<Color>();

        #region monitoring fields

        private int oldWidth,
                    oldHeight,
                    oldRoundToNearest,
                    oldNumData;
        private float oldPipLength,
                      oldEdgeOffset,
                      oldLabelOffset,
                      oldXAxisLabelFrequency,
                      oldYAxisLabelFrequency,
                      oldValueFrequency;
        private bool oldDrawGrid,
                     oldRoundAxisValues,
                     oldAutoGenXLabels,
                     oldAutoGenYLabels,
                     oldFitWidth,
                     oldFitHeight,
                     oldFitBoth;

        #endregion

        //private VectorLine gridLine = new VectorLine("Grid", new List<Vector3>(), 1, LineType.Continuous);

        // Start is called before the first frame update
        void Start()
        {
            cvsGraphCanvas = gameObject.GetComponent<Canvas>();
            if (cvsGraphCanvas == null)
                throw new MissingComponentException("Missing canvas for graph");
            //Set fields used to check for changes in graph
            SetMonitoringFields();

            rctGraphTransform = GetComponent<RectTransform>();

            GetGraphValues();
            if (GraphValues.Count > 0)
                DrawGraph();
            else
                Debug.LogError("No graph values");       
        }

        private void SetMonitoringFields()
        {
            oldWidth = Width;
            oldHeight = Height;
            oldRoundToNearest = RoundToNearest;
            oldNumData = GraphValues.Count;
            oldPipLength = PipLength;
            oldEdgeOffset = EdgeOffset;
            oldLabelOffset = LabelOffsetFromAxis;
            oldXAxisLabelFrequency = XAxisTextLabelFrequency;
            oldYAxisLabelFrequency = YAxisTextLabelFrequency;
            oldValueFrequency = ValueFrequency;
            oldDrawGrid = BackgroundGrid;
            oldRoundAxisValues = RoundAxisValues;
            oldAutoGenXLabels = AutoGenerateXLabels;
            oldAutoGenYLabels = AutoGenerateYLabels;
            oldFitWidth = FitToWidth;
            oldFitHeight = FitToHeight;
            oldFitBoth = FitToBoth;
        }

        private void Update()
        {
            #region monitoring fields for graph value changes

            if(oldWidth != Width ||
                oldHeight != Height)
            {
                if(Width > 0 && Height > 0)
                {
                    Resize();
                    SetMonitoringFields();
                }                  
            }

            if(oldFitHeight != FitToHeight ||
                oldFitWidth != FitToWidth || 
                oldFitBoth != FitToBoth)
            {
                DrawGraph();
                SetMonitoringFields();
            }

            if( oldRoundToNearest != RoundToNearest ||
                oldNumData != GraphValues.Count ||
                oldPipLength != PipLength ||
                oldEdgeOffset != EdgeOffset ||
                oldLabelOffset != LabelOffsetFromAxis ||
                oldXAxisLabelFrequency != XAxisTextLabelFrequency ||
                oldYAxisLabelFrequency != YAxisTextLabelFrequency ||
                oldValueFrequency != ValueFrequency ||
                oldDrawGrid != BackgroundGrid ||
                oldRoundAxisValues != RoundAxisValues ||
                oldAutoGenXLabels != AutoGenerateXLabels ||
                oldAutoGenYLabels != AutoGenerateYLabels)
            {                
                if(XAxisTextLabelFrequency > 0 &&
                    YAxisTextLabelFrequency > 0 &&
                    ValueFrequency > 0)
                {
                    GetGraphValues();
                    DrawGraph();

                    SetMonitoringFields();
                }                
            }

            #endregion
        }

        private void GetGraphValues()
        {
            GraphValues.Clear();
            lineColours.Clear();

            //setup graph data           
            if (GraphFromEquation && 
                Type == Graphio_GraphType.Line && 
                Equations?.Count > 0)
            {
                Graphio_Equations.Clear();
                foreach (var func in Equations)
                {
                    if (func.function != null)
                    {
                        Graphio_Equations.Add(ParseEquation(func.function));
                        lineColours.Add(func.colour);
                    }
                    else
                    {
                        Debug.LogError("function string null");
                    }                      
                }

                //with equation parsed and steps drawn up, solve for values             
                foreach (Graphio_Equation equation in Graphio_Equations)
                {
                    GraphValues.Add(GetGraphValues(equation));
                }                
            }
            //Everything other than equation graphs
            else
            {
                if (Data?.Count > 0)
                {
                    GraphFromEquation = false;//just in case this was left on in the editor then no equations given
                    foreach(var _data in Data)
                    {
                        GraphValues.Add(new List<Vector2> (_data.list));
                        lineColours.Add(_data.colour);
                    }                   
                }
                else
                {
                    //TODO: no data
                }
            }
        }

        private void DrawGraph()
        {          
            //clear any lines currently on the graph
            for(int i = 0; i < currentLines.Count; i++)
            {
                var line = currentLines[i];
                VectorLine.Destroy(ref line);
            }
            currentLines.Clear();
            for(int i = 0; i < currentGraphExtras.Count; i++)
            {
                Destroy(currentGraphExtras[i]);
            }
            currentGraphExtras.Clear();

            float minX = 0, minY = 0, maxX = 0, maxY = 0; //smallest and largest values for each axis in the lists of values

            float _xLength = 0f, _yLength = 0f;

            int numPosXValues = 0, numNegXValues = 0, numPosYValues = 0, numNegYValues = 0;

            //Work out dimensions of graph
            switch (Type)
            {
                case Graphio_GraphType.Line:
                    //find axis edges
                    for (int graphNum = 0; graphNum < GraphValues.Count; graphNum++)
                    {
                        for (int i = 0; i < GraphValues[graphNum].Count; i++)
                        {
                            if (GraphValues[graphNum][i].x > maxX)
                                maxX = GraphValues[graphNum][i].x;
                            else if (GraphValues[graphNum][i].x < minX)
                                minX = GraphValues[graphNum][i].x;

                            if (GraphValues[graphNum][i].y > maxY)
                                maxY = GraphValues[graphNum][i].y;
                            else if (GraphValues[graphNum][i].y < minY)
                                minY = GraphValues[graphNum][i].y;
                        }
                    }

                    if (RoundAxisValues)
                    {
                        float _remainder = minX % RoundToNearest;
                        float _result = 0;

                        if (_remainder != 0)
                        {
                            _result = minX - _remainder;
                            if (_result < 0)
                            {
                                minX = _result - RoundToNearest;
                            }
                            else if (_result >= 0)
                            {
                                minX = _result + RoundToNearest;
                            }
                        }

                        _remainder = maxX % RoundToNearest;
                        if (_remainder != 0)
                        {
                            _result = maxX - _remainder;
                            if (_result < 0)
                            {
                                maxX = _result - RoundToNearest;
                            }
                            else if (_result >= 0)
                            {
                                maxX = _result + RoundToNearest;
                            }
                        }

                        _remainder = minY % RoundToNearest;

                        if (_remainder != 0)
                        {
                            _result = minY - _remainder;
                            if (_result < 0)
                            {
                                minY = _result - RoundToNearest;
                            }
                            else if (_result >= 0)
                            {
                                minY = _result + RoundToNearest;
                            }
                        }

                        _remainder = maxY % RoundToNearest;

                        if (_remainder != 0)
                        {
                            _result = maxY - _remainder;
                            if (_result < 0)
                            {
                                maxY = _result - RoundToNearest;
                            }
                            else if (_result >= 0)
                            {
                                maxY = _result + RoundToNearest;
                            }
                        }
                    }

                    numPosXValues = (int)Math.Ceiling(maxX / StepX);
                    numNegXValues = (int)Math.Ceiling(-minX / StepX);
                    numPosYValues = (int)Math.Ceiling(maxY / StepY);
                    numNegYValues = (int)Math.Ceiling(-minY / StepY);

                    //The x and y lengths of the graph
                    _yLength = (maxY - minY);
                    _xLength = (maxX - minX);

                    break;
                case Graphio_GraphType.Bar:
                    //figure out width of graph
                    _xLength = GraphValues?.Count * (BarChartBarWidth + BarChartBarSeperator) ?? 0f * (BarChartBarWidth + BarChartBarSeperator);
                    _yLength = 0;
                    for(int i = 0; i < GraphValues.Count; i++)
                    {
                        if (GraphValues?[i]?[0].y > _yLength)
                            _yLength = GraphValues[i][0].y;
                    }
                    if (RoundAxisValues)
                    {
                        float _result = 0;
                        float _remainder = _yLength % RoundToNearest;

                        if (_remainder != 0)
                        {
                            _result = _yLength - _remainder;
                            if (_result < 0)
                            {
                                _yLength = _result - RoundToNearest;
                            }
                            else if (_result >= 0)
                            {
                                _yLength = _result + RoundToNearest;
                            }
                        }
                    }
                    break;
            }
    
            //get scale
            if (FitToWidth)
            {
                Scale = (Width - (2 * EdgeOffset)) / _xLength;
            }
            else if (FitToHeight)
            {
                Scale = (Height - (2 * EdgeOffset)) / _yLength;
            }
            else //fit to current dimensions
            {
                FitToBoth = true;
                xScale = (Width - (2 * EdgeOffset)) / _xLength;
                yScale = (Height - (2 * EdgeOffset)) / _yLength;
            }

            var _cvs = gameObject.GetComponent<Canvas>();

            //background canvas
            if (rctGraphTransform != null)
            {
                if (FitToWidth)
                {
                    Height = (int)((int)(_yLength * Scale) + (2 * EdgeOffset));
                }
                else if (FitToHeight)
                {
                    Width = (int)((int)(_xLength * Scale) + (2 * EdgeOffset));
                }

                rctGraphTransform.sizeDelta = new Vector2(Width, Height);
            }

            var gridLine = new VectorLine("Grid", new List<Vector2>(), GridLineWidth, LineType.Discrete)
            {
                color = new Color(0.3f, 0.3f, 0.3f, 0.5f)
            };
            gridLine.SetCanvas(Canvas, false);

            var _numXGridVals = 0; 
            var _numYGridVals = 0;
            //get number of values on each axis for drawing grid 
            switch (Type)
            {
                case Graphio_GraphType.Line:
                    _numXGridVals = numNegXValues + numPosXValues;
                    _numXGridVals = numNegYValues + numPosYValues;
                    break;
                case Graphio_GraphType.Bar:
                    _numXGridVals = GraphValues?.Count ?? 0;
                    _numYGridVals = (int)(_yLength / StepY);
                    break;
            }

            //background grid
            if (BackgroundGrid)
            {
                //only need to draw vertical lines if line chart
                if (Type == Graphio_GraphType.Line)
                {
                    for (int x = 0; x <= _numXGridVals; x++)
                    {
                        //whether to fit to custom dimensions or to width or height
                        if (FitToBoth)
                        {
                            gridLine.points2.Add(new Vector2(
                                EdgeOffset + ((x * StepX) * xScale),
                                EdgeOffset));
                            gridLine.points2.Add(new Vector2(
                                EdgeOffset + ((x * StepX) * xScale),
                                Height - EdgeOffset));
                        }
                        else
                        {
                            gridLine.points2.Add(new Vector2(
                                EdgeOffset + (x * StepX * Scale),
                                EdgeOffset));
                            gridLine.points2.Add(new Vector2(
                                EdgeOffset + (x * StepX * Scale),
                                EdgeOffset + (_yLength * Scale)));
                        }
                    }
                }
                
                for(int y = 0; y <= _numYGridVals; y++)
                {
                    //whether to fit to custom dimensions or to width or height
                    if (FitToBoth)
                    {
                        gridLine.points2.Add(new Vector2(
                        EdgeOffset,
                        EdgeOffset + (y * StepY * yScale)));
                        gridLine.points2.Add(new Vector2(
                            Width - EdgeOffset,
                            EdgeOffset + (y * StepY * yScale)));
                    }
                    else
                    {
                        gridLine.points2.Add(new Vector2(
                        EdgeOffset,
                        EdgeOffset + (y * StepY * Scale)));
                        gridLine.points2.Add(new Vector2(
                            EdgeOffset + (_xLength * Scale),
                            EdgeOffset + (y * StepY * Scale)));
                    }                    
                }
            }

            gridLine.Draw();
            currentLines.Add(gridLine);

            List<Vector2> _xAxisPoints = new List<Vector2>();
            List<Vector2> _yAxisPoints = new List<Vector2>();

            float _currentScale = 1;
            int _labelCount = 0;
            //Draw graph values
            switch (Type)
            {
                case Graphio_GraphType.Line:
                    {
                        //Offset the line point coordinates by this amount to make room for the negative side of the axis
                        float xOffset = numNegXValues * StepX;
                        float yOffset = numNegYValues * StepY;

                        //Start at the negative end of the x axis, work towards postive end, use step as increment
                        _labelCount = 0;
                        //add x axis points and labels
                        _currentScale = 1;
                        if (FitToBoth)
                            _currentScale = xScale;
                        else
                            _currentScale = Scale;
                        for (float x = -(numNegXValues * StepX); x <= numPosXValues * StepX; x += StepX, _labelCount++)
                        {
                            _xAxisPoints.Add(new Vector2(
                                EdgeOffset + (x + xOffset) * _currentScale,
                                EdgeOffset + yOffset * _currentScale));

                            //If writing a label at this pip, draw bigger pip and spawn text
                            if (AutoGenerateXLabels)
                            {
                                if (_labelCount % XAxisTextLabelFrequency == 0 || _labelCount == 0)
                                {
                                    _xAxisPoints.Add(new Vector2(
                                        EdgeOffset + (x + xOffset) * _currentScale,
                                        EdgeOffset + (yOffset + PipLength * 2) * _currentScale));

                                    var _go = Instantiate(LabelTemplate, gameObject.transform, false);
                                    _go.transform.localPosition = new Vector3(
                                        EdgeOffset + (x + xOffset) * _currentScale - LabelCorrectionAmount,
                                        EdgeOffset + (yOffset * _currentScale - LabelOffsetFromAxis),
                                        0);
                                    _go.GetComponent<Text>().text = x.ToString();
                                    _go.SetActive(true);
                                    currentGraphExtras.Add(_go);

                                } // if no label just draw pip 
                                else _xAxisPoints.Add(new Vector2(
                                    EdgeOffset + (x + xOffset) * _currentScale,
                                    EdgeOffset + (yOffset + PipLength) * _currentScale));
                            }
                            else
                            {
                                if (XLabels.Value.Contains(x.ToString()))
                                {
                                    _xAxisPoints.Add(new Vector2(
                                        EdgeOffset + (x + xOffset) * _currentScale,
                                        EdgeOffset + (yOffset + PipLength * 2) * _currentScale));

                                    var _go = Instantiate(LabelTemplate, gameObject.transform, false);
                                    _go.transform.localPosition = new Vector3(
                                        EdgeOffset + (x + xOffset) * _currentScale - LabelCorrectionAmount,
                                        EdgeOffset + (yOffset * _currentScale - LabelOffsetFromAxis),
                                        0);
                                    _go.GetComponent<Text>().text = x.ToString();
                                    _go.SetActive(true);
                                    currentGraphExtras.Add(_go);
                                }
                            }
                            _xAxisPoints.Add(new Vector2(
                                EdgeOffset + (x + xOffset) * _currentScale,
                                EdgeOffset + yOffset * _currentScale));
                        }
                        _labelCount = 0;
                        if (FitToBoth)
                            _currentScale = yScale;
                        else
                            _currentScale = Scale;
                        //add y axis points and labels
                        for (float y = -(numNegYValues * StepY); y <= numPosYValues * StepY; y += StepY, _labelCount++)
                        {
                            _yAxisPoints.Add(new Vector2(
                                EdgeOffset + xOffset * _currentScale,
                                EdgeOffset + (y + yOffset) * _currentScale));

                            if (AutoGenerateYLabels)
                            {
                                if (_labelCount % YAxisTextLabelFrequency == 0 || _labelCount == 0)
                                {
                                    _yAxisPoints.Add(new Vector2(
                                        EdgeOffset + (xOffset + PipLength * 2) * _currentScale,
                                        EdgeOffset + (y + yOffset) * _currentScale));

                                    var _go = Instantiate(LabelTemplate, gameObject.transform, false);
                                    _go.transform.localPosition = new Vector3(
                                        EdgeOffset + (xOffset * _currentScale - LabelOffsetFromAxis),
                                        EdgeOffset + (y + yOffset) * _currentScale - LabelCorrectionAmount,
                                        0);
                                    _go.GetComponent<Text>().text = y.ToString();
                                    _go.SetActive(true);
                                    currentGraphExtras.Add(_go);

                                    _yAxisPoints.Add(new Vector2(
                                        EdgeOffset + xOffset * _currentScale,
                                        EdgeOffset + (y + yOffset) * _currentScale));
                                }
                                else _yAxisPoints.Add(new Vector2(
                                    EdgeOffset + (xOffset + PipLength) * _currentScale,
                                    EdgeOffset + (y + yOffset) * _currentScale));
                            }
                            else
                            {
                                if (YLabels.Value.Contains(y.ToString()))
                                {
                                    _yAxisPoints.Add(new Vector2(
                                        EdgeOffset + (xOffset + PipLength * 2) * _currentScale,
                                        EdgeOffset + (y + yOffset) * _currentScale));

                                    var _go = Instantiate(LabelTemplate, gameObject.transform, false);
                                    _go.transform.localPosition = new Vector3(
                                        EdgeOffset + (xOffset * _currentScale - LabelOffsetFromAxis),
                                        EdgeOffset + (y + yOffset) * _currentScale - LabelCorrectionAmount,
                                        0);
                                    _go.GetComponent<Text>().text = y.ToString();
                                    _go.SetActive(true);
                                    currentGraphExtras.Add(_go);

                                    _yAxisPoints.Add(new Vector2(
                                        EdgeOffset + xOffset * _currentScale,
                                        EdgeOffset + (y + yOffset) * _currentScale));
                                }
                            }

                            
                        }                       
                        //draw graph data lines
                        for (int i = 0; i < GraphValues.Count; i++)
                        {
                            // need to scale graph values
                            for(int x = 0; x < GraphValues[i].Count; x++)
                            {
                                if (FitToBoth)
                                {
                                    GraphValues[i][x] = new Vector2(
                                        EdgeOffset + (GraphValues[i][x].x + xOffset) * xScale,
                                        EdgeOffset + (GraphValues[i][x].y + yOffset) * yScale);
                                }
                                else
                                {
                                    GraphValues[i][x] = new Vector2(
                                        EdgeOffset + (GraphValues[i][x].x + xOffset) * Scale,
                                        EdgeOffset + (GraphValues[i][x].y + yOffset) * Scale);
                                }
                            }                           

                            VectorLine _line = new VectorLine("Graph Line", GraphValues[i], 1f, LineType.Continuous)
                            {
                                color = lineColours[i],
                                joins = Joins.Fill
                            };
                            _line.SetCanvas(_cvs, false);
                            _line.Draw();
                            currentLines.Add(_line);
                        }

                        break;
                    }

                case Graphio_GraphType.Bar:
                    {
                        _currentScale = 1;
                        _labelCount = 0;

                        if (FitToBoth)
                            _currentScale = yScale;
                        _yAxisPoints.Add(new Vector2(EdgeOffset, EdgeOffset));

                        if (FitToBoth)
                            _currentScale = xScale;
                        else
                            _currentScale = Scale;
                        //draw the axis
                        _xAxisPoints.Add(new Vector2(EdgeOffset, EdgeOffset));
                        _xAxisPoints.Add(new Vector2(Width - EdgeOffset, EdgeOffset));

                        if (FitToBoth)
                            _currentScale = yScale;

                        for (float i = 1; i <= _yLength; i += StepY, _labelCount++)
                        {
                            _yAxisPoints.Add(new Vector2(EdgeOffset, EdgeOffset + (i * _currentScale)));
                            _yAxisPoints.Add(new Vector2(EdgeOffset + (PipLength * _currentScale), EdgeOffset + (i * _currentScale)));

                            //Spawn label
                            if (AutoGenerateYLabels)
                            {
                                if (_labelCount % YAxisTextLabelFrequency == 0 || _labelCount == 0)
                                {
                                    var _go = Instantiate(LabelTemplate, gameObject.transform, false);
                                    _go.transform.localPosition = new Vector3(
                                        EdgeOffset - LabelOffsetFromAxis,
                                        EdgeOffset + (i * _currentScale) - LabelCorrectionAmount,
                                        0);
                                    _go.GetComponent<Text>().text = i.ToString();
                                    _go.SetActive(true);
                                    currentGraphExtras.Add(_go);
                                }
                            }
                            else
                            {
                                if (YLabels.Value.Contains(i.ToString()))
                                {
                                    var _go = Instantiate(LabelTemplate, gameObject.transform, false);
                                    _go.transform.localPosition = new Vector3(
                                        EdgeOffset - LabelOffsetFromAxis,
                                        EdgeOffset + (i * _currentScale) - LabelCorrectionAmount,
                                        0);
                                    _go.GetComponent<Text>().text = i.ToString();
                                    _go.SetActive(true);
                                    currentGraphExtras.Add(_go);
                                }
                            }

                            _yAxisPoints.Add(new Vector2(EdgeOffset, EdgeOffset + (i * _currentScale)));
                        }


                        //draw the bars                   
                        float _barPos = BarChartBarSeperator / 2; //half of seperator gap on left side, and same on right end
                        for (int i = 0; i < GraphValues?.Count; i++, _barPos += BarChartBarWidth + BarChartBarSeperator)
                        {

                            GameObject _barObj = Instantiate(BarChartBar, gameObject.transform, false);
                            GameObject _label = Instantiate(LabelTemplate, gameObject.transform, false);

                            if (FitToBoth)
                                _currentScale = xScale;

                            _barObj.transform.localPosition = new Vector3(
                                EdgeOffset + (_barPos * _currentScale),
                                EdgeOffset,
                                0);
                            _label.transform.localPosition = new Vector3(
                                EdgeOffset + (_barPos * _currentScale),
                                0,
                                0);

                            if (FitToBoth)
                                _currentScale = yScale;

                            _barObj.GetComponent<RectTransform>().sizeDelta = new Vector2(BarChartBarWidth, GraphValues[i][0].y * _currentScale);

                            currentGraphExtras.Add(_barObj);
                            currentGraphExtras.Add(_label);
                            _label.GetComponent<RectTransform>().sizeDelta = new Vector2(BarChartBarWidth + (BarChartBarSeperator / 2), 15);
                            _label.GetComponent<Text>().text = XLabels.Value?[i] ?? "";
                            _label.SetActive(true);

                            _barObj.SetActive(true);
                            _barObj.GetComponent<Image>().color = lineColours?[i] ?? Color.white;
                        }
                    }
                    break;
            }

            //draw axis
            VectorLine _xAxis = new VectorLine("X Axis", _xAxisPoints, 1)
            {
                color = Color.black,
                lineType = LineType.Continuous
            };
            VectorLine _yAxis = new VectorLine("Y Axis", _yAxisPoints, 1)
            {
                color = Color.black,
                lineType = LineType.Continuous              
            };
            _xAxis.SetCanvas(_cvs, false);
            _yAxis.SetCanvas(_cvs, false);

            _xAxis.Draw();
            _yAxis.Draw();

            currentLines.Add(_xAxis);
            currentLines.Add(_yAxis);
        }

        private void Resize()
        {
            if (FitToWidth)
            {
                Scale = (float)Width / oldWidth; 
            }
            else if (FitToHeight)
            {
                Scale = (float)Height / oldHeight;
            }
            else
            {
                FitToBoth = true;
                xScale = (float)Width / oldWidth;
                yScale = (float)Height / oldHeight;           
            }
            //TODO: just make the rectangle the size you want i.e. use width and height, no need for scale. figure out why scaling doesn't work (is it because using (2*edgeoffset)???)
            if (FitToBoth)           
                rctGraphTransform.sizeDelta = new Vector2(rctGraphTransform.sizeDelta.x * xScale, rctGraphTransform.sizeDelta.y * yScale);           
            else
                rctGraphTransform.sizeDelta = new Vector2(rctGraphTransform.sizeDelta.x * Scale, rctGraphTransform.sizeDelta.y * Scale);

            foreach(var _gObj in currentGraphExtras)
            {
                if(FitToBoth)
                    _gObj.transform.localPosition = new Vector3(_gObj.transform.localPosition.x * xScale, _gObj.transform.localPosition.y * yScale, _gObj.transform.localPosition.z);
                else
                    _gObj.transform.localPosition = new Vector3(_gObj.transform.localPosition.x * Scale, _gObj.transform.localPosition.y * Scale, _gObj.transform.localPosition.z);
            }

            foreach (var _line in currentLines)
            {
                for(int i = 0; i < _line.points2.Count; i++)
                {
                    if(FitToBoth)
                        _line.points2[i] = new Vector2(_line.points2[i].x * xScale, _line.points2[i].y * yScale);
                    else
                        _line.points2[i] = new Vector2(_line.points2[i].x * Scale, _line.points2[i].y * Scale);
                    _line.Draw();
                }
            }
        }

        //function to parse string to EquationInstruction
        internal Graphio_Equation ParseEquation(string equation)
        {           
            //Checks for valid 
            if(equation.Length < 3)
            {
                return default;//TODO: throw error
            }

            string _leftSide = "";
            string _rightSide = "";
            int _equalsIndex = 0;

            for(int i = 0; i < equation.Length; i++)
            {
                if(equation[i] == '=')
                {
                    _equalsIndex = i;
                    _leftSide = equation.Substring(0, i);
                    _rightSide = equation.Substring(i + 1);
                }

                if(i == equation.Length-1 && equation[i] != '=' && _equalsIndex == 0)
                {
                    //there is no equals sign in this equation
                    //TODO: throw exception
                    return default;
                }
            }

            bool isXEqualsEq;//if format is x=y or y=x
            if (_leftSide.Contains("x") || equation.Contains("X"))
                isXEqualsEq = true;//this is an equation of the x=y format
            else if (_leftSide.Contains("y") || _leftSide.Contains("Y"))
                isXEqualsEq = false;//this is an equation of the y=x format
            else return default; //this isn't the correct format

            if (isXEqualsEq)
            {
                if (!_rightSide.Contains("y") && !_rightSide.Contains("Y"))
                    return default; //second part of equation doesn't contain Y
                //TODO: throw error
            }
            else
            {
                if (!_rightSide.Contains("x") && !_rightSide.Contains("x"))
                    return default; //second part of equation doesn't contain X
                //TODO: throw error 
            }
            //Turn equation into list of 'parts'
            var _equationParts = new List<Graphio_EquationPart>();
   
            //Iterate over string to parse it to EquationPart list
            for(int i = 0; i < equation.Length; i++)
            {
                switch (equation[i].GetEquationPartType())
                {
                    case Graphio_EquationPartType.Number:
                        _equationParts.Add(ParseNumber(equation, i));
                        break;
                    case Graphio_EquationPartType.Variable:
                        _equationParts.Add(new Graphio_EquationPart(Graphio_EquationPartType.Variable, variable: equation[i].GetVariable()));
                        break;
                    case Graphio_EquationPartType.Operation:
                        if(equation[i].GetEquationOperation() == Graphio_EquationOperation.Sub)
                        {
                            //Check whether this is actually subtraction or whether it signals a negative number or variable
                            bool isNegative = false;
                            if (i == 0)
                                isNegative = true;
                            // check thing to right is number or variable, or open bracket
                            else if (equation[i - 1].GetEquationPartType() == Graphio_EquationPartType.Equals || equation[i - 1].GetEquationPartType() == Graphio_EquationPartType.Operation)
                                isNegative = true;
                            else if (equation[i - 1].GetEquationPartType() == Graphio_EquationPartType.Parenthesis &&
                                equation[i - 1].GetParenthesisType() == Graphio_ParenthesisType.Open)
                                isNegative = true;

                            if (isNegative)
                            {
                                //if there is something to the right of this index
                                if(i + 1 != equation.Length)
                                {
                                    if(equation[i + 1].GetEquationPartType() == Graphio_EquationPartType.Number)
                                    {
                                        _equationParts.Add(ParseNumber(equation, i));
                                        i += _equationParts[_equationParts.Count - 1].Value.ToString().Length; // move the loop forward so we don't try and parse the number again
                                    }
                                    else if (equation[i + 1].GetEquationPartType() == Graphio_EquationPartType.Variable)
                                    {
                                        _equationParts.Add(new Graphio_EquationPart(Graphio_EquationPartType.Variable, variable: equation[i + 1].GetVariable(), isNegative: true));
                                        i++;
                                    }
                                }
                            }
                            else _equationParts.Add(new Graphio_EquationPart(Graphio_EquationPartType.Operation, operation: equation[i].GetEquationOperation()));                    
                        }
                        else _equationParts.Add(new Graphio_EquationPart(Graphio_EquationPartType.Operation, operation: equation[i].GetEquationOperation()));
                        break;
                    case Graphio_EquationPartType.Parenthesis:
                        _equationParts.Add(new Graphio_EquationPart(Graphio_EquationPartType.Parenthesis, parenthesis: equation[i].GetParenthesisType()));
                        break;
                    case Graphio_EquationPartType.Equals:
                        _equationParts.Add(new Graphio_EquationPart(Graphio_EquationPartType.Equals));
                        break;
                    default:
                        break;
                }
            }

            //Check for implicit multiplication and add it to list
            for (int i = 0; i < _equationParts.Count; i++)
            {
                if (_equationParts[i].ContentType == Graphio_EquationPartType.Parenthesis)
                {
                    if (_equationParts[i].Parenthesis == Graphio_ParenthesisType.Open)
                    {//only check if we're not at the beginning of the equation
                        if (i > 0)
                        {
                            // if the part to the left is either a number, variable or close parenthesis, add mult sign
                            if (_equationParts[i - 1].ContentType == Graphio_EquationPartType.Number || _equationParts[i - 1].ContentType == Graphio_EquationPartType.Variable)
                            {
                                _equationParts.Insert(i, new Graphio_EquationPart(Graphio_EquationPartType.Operation, operation: Graphio_EquationOperation.Mult));
                                i++;
                            }
                            else if(_equationParts[i - 1].ContentType == Graphio_EquationPartType.Parenthesis)
                            {
                                if (_equationParts[i - 1].Parenthesis == Graphio_ParenthesisType.Close)
                                {
                                    _equationParts.Insert(i, new Graphio_EquationPart(Graphio_EquationPartType.Operation, operation: Graphio_EquationOperation.Mult));
                                    i++;
                                }
                            }
                        }
                    }
                    else // it must be a close parenthesis
                    {//only check if we're not at the end of the equation
                        if (i + 1 < _equationParts.Count)
                        {
                            if (_equationParts[i + 1].ContentType == Graphio_EquationPartType.Number || _equationParts[i + 1].ContentType == Graphio_EquationPartType.Variable)
                            {
                                _equationParts.Insert(i + 1, new Graphio_EquationPart(Graphio_EquationPartType.Operation, operation: Graphio_EquationOperation.Mult));
                            }
                            else if (_equationParts[i + 1].ContentType == Graphio_EquationPartType.Parenthesis)
                            {
                                if (_equationParts[i + 1].Parenthesis == Graphio_ParenthesisType.Open)
                                {
                                    _equationParts.Insert(i + 1, new Graphio_EquationPart(Graphio_EquationPartType.Operation, operation: Graphio_EquationOperation.Mult));                                    
                                }
                            }
                        }
                    }                    
                }
                else if(_equationParts[i].ContentType == Graphio_EquationPartType.Variable)
                {
                    //if there is something to the left of this index
                    if(i > 0)
                    {
                        if (_equationParts[i - 1].ContentType == Graphio_EquationPartType.Number || _equationParts[i - 1].ContentType == Graphio_EquationPartType.Variable)
                        {
                            _equationParts.Insert(i, new Graphio_EquationPart(Graphio_EquationPartType.Operation, operation: Graphio_EquationOperation.Mult));
                            i++;
                        }
                        else if(_equationParts[i - 1].ContentType == Graphio_EquationPartType.Parenthesis)
                        {
                            if (_equationParts[i - 1].Parenthesis == Graphio_ParenthesisType.Close)
                            {
                                _equationParts.Insert(i, new Graphio_EquationPart(Graphio_EquationPartType.Operation, operation: Graphio_EquationOperation.Mult));
                                i++;
                            }
                        }
                    }
                    //if there is something to the right of this index
                    if(i + 1 < _equationParts.Count)
                    {
                        if (_equationParts[i + 1].ContentType == Graphio_EquationPartType.Number || _equationParts[i + 1].ContentType == Graphio_EquationPartType.Variable)
                        {
                            _equationParts.Insert(i + 1, new Graphio_EquationPart(Graphio_EquationPartType.Operation, operation: Graphio_EquationOperation.Mult));
                            i += 2;
                        }
                        else if (_equationParts[i + 1]. ContentType == Graphio_EquationPartType.Parenthesis)
                        {
                            if(_equationParts[i + 1].Parenthesis == Graphio_ParenthesisType.Open)
                            {
                                _equationParts.Insert(i + 1, new Graphio_EquationPart(Graphio_EquationPartType.Operation, operation: Graphio_EquationOperation.Mult));
                                i += 2;
                            }
                        }
                    }
                }
            }

            var _solvedEquation = CreateSteps(_equationParts, stringEquation: equation);
            return new Graphio_Equation(_equationParts, _solvedEquation.Steps, equation, isXEqualsEq);
        }

        /// <summary>
        /// Parse a number that starts at a certain index, can also parse negatives if index is a - sign
        /// </summary>
        /// <param name="equation"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        private Graphio_EquationPart ParseNumber(string equation, int index)
        {
            float _number;
            bool isFirstCharNegative = false;
            if(equation[index] == '-')
            {
                isFirstCharNegative = true;
            }
            if (index + 1 != equation.Length)
            {
                int x = index;
                if (isFirstCharNegative)
                    x++; // if the first char of this number is a - sign then don't check if it's a number
                do
                {
                    x++;
                } while (char.IsNumber(equation[x]));

                var _numSubStr = equation.Substring(index, x - index);

                if (float.TryParse(_numSubStr, out _number)) { }
                else
                {
                    //TOOD: throw error;
                }
            }
            else
            {
                if (float.TryParse(equation[index].ToString(), out _number)) { }
                else
                {
                    //TODO: throw error
                }
            }

            return new Graphio_EquationPart(Graphio_EquationPartType.Number, value: _number);
        }

        private Graphio_Equation CreateSteps(List<Graphio_EquationPart> equation, List<Graphio_EquationStep> _stepsList = null, string stringEquation = "", int indexOffset = 0, bool needRemoveParentheses = false)
        {
            if(_stepsList == null)
                _stepsList = new List<Graphio_EquationStep>();

            //Create a temporary equation part list to operate on to get steps
            var _tempPartList = new List<Graphio_EquationPart>(equation);

            //Find equals sign and solve right side first
            for(int i = 0; i < _tempPartList.Count; i++)
            {
                if(_tempPartList[i].ContentType == Graphio_EquationPartType.Equals)
                {
                    var _result = CreateSteps(_tempPartList.GetRange(i+1, _tempPartList.Count - (i + 1)), _stepsList, indexOffset: indexOffset + i + 1);
                    _tempPartList.RemoveRange(i + 1, _tempPartList.Count - (i + 1));
                    _tempPartList.AddRange(_result.EquationParts);
                }
            }
            //BODMAS
            //Brackets
            bool _closeBracketFound = true;
            for (int i = 0; i < _tempPartList.Count; i++)
            {
                if(_tempPartList[i].ContentType == Graphio_EquationPartType.Parenthesis)
                {
                    if(_tempPartList[i].Parenthesis == Graphio_ParenthesisType.Open)
                    {
                        _closeBracketFound = false;
                        int numCloseBracketsToSkip = 0;//increment this every time we find another open bracket so we know how many closed brackets to find
                        for (int o = i + 1; o < _tempPartList.Count; o++)
                        {
                            //if bracket found, check type of bracket, then recursively get steps
                            if (_tempPartList[o].ContentType == Graphio_EquationPartType.Parenthesis)
                            {
                                if (_tempPartList[o].Parenthesis == Graphio_ParenthesisType.Open)
                                    numCloseBracketsToSkip++;
                                else //If closed bracket
                                {
                                    if (numCloseBracketsToSkip > 0)
                                        numCloseBracketsToSkip--;
                                    else // if this is the closed bracket that we're looking for
                                    {
                                        _closeBracketFound = true;
                                        //check brackets contain at least one operator, otherwise just remove the brackets
                                        bool _containsOperator = false;
                                        for (int y = i + 1; y < o; y++)
                                        {
                                            if (_tempPartList[y].ContentType == Graphio_EquationPartType.Operation)
                                            {
                                                _containsOperator = true;
                                                break;
                                            }
                                        }
                                        if (_containsOperator)
                                        {
                                            var _parenthesisPartsToSolve = new List<Graphio_EquationPart>(_tempPartList.GetRange(i + 1, o - i - 1));
                                            var _solvedParenthesis = CreateSteps(_parenthesisPartsToSolve, _stepsList, indexOffset: indexOffset + i + 1, needRemoveParentheses: true); ;

                                            //remove solved parenthesis and replace it with solution
                                            _tempPartList.RemoveRange(i, o - i + 1);
                                            _tempPartList.InsertRange(i, _solvedParenthesis.EquationParts);
                                        }
                                        else
                                        {
                                            _stepsList.Add(new Graphio_EquationStep(Graphio_EquationOperation.RemoveParentheses, o + indexOffset, i + indexOffset));

                                            PerformOperation(_tempPartList, Graphio_EquationOperation.RemoveParentheses, o, i);
                                        }
                                    }
                                }
                            }
                        }
                    }                    
                }

                if (!_closeBracketFound)
                {
                    Debug.Log("Error close bracket not found");
                    return default;
                }
            }

            //Orders
            CheckForOperation(_tempPartList, _stepsList, Graphio_EquationOperation.Pow, indexOffset, needRemoveParentheses);

            //Division
            CheckForOperation(_tempPartList, _stepsList, Graphio_EquationOperation.Div, indexOffset, needRemoveParentheses);

            //Multiplication
            CheckForOperation(_tempPartList, _stepsList, Graphio_EquationOperation.Mult, indexOffset, needRemoveParentheses);

            //Addition
            CheckForOperation(_tempPartList, _stepsList, Graphio_EquationOperation.Add, indexOffset, needRemoveParentheses);

            //Subtraction
            CheckForOperation(_tempPartList, _stepsList, Graphio_EquationOperation.Sub, indexOffset, needRemoveParentheses);

            return new Graphio_Equation(_tempPartList, _stepsList, stringEquation);
        }

        private void PerformOperation(List<Graphio_EquationPart> parts, Graphio_EquationOperation operation, int operatorIndex, int secondaryIndex = 0)
        {
            float _result = 0;
            float _left = 0;
            float _right = 0;

            if (operation != Graphio_EquationOperation.RemoveParentheses)
            {               
                // Check for variables
                if (parts[operatorIndex - 1].ContentType == Graphio_EquationPartType.Variable)
                {
                    switch (parts[operatorIndex - 1].Variable)
                    {
                        case Graphio_EquationVariable.X:
                            if (parts[operatorIndex - 1].IsNegativeVariableValue)
                                _left = -XValue;
                            else
                                _left = XValue;
                            break;
                        case Graphio_EquationVariable.Y:
                            if (parts[operatorIndex - 1].IsNegativeVariableValue)
                                _left = -YValue;
                            else
                                _left = YValue;
                            break;
                    }
                }
                else
                {
                    _left = parts[operatorIndex - 1].Value;
                }
                if (parts[operatorIndex + 1].ContentType == Graphio_EquationPartType.Variable)
                {
                    switch (parts[operatorIndex + 1].Variable)
                    {
                        case Graphio_EquationVariable.X:
                            if (parts[operatorIndex + 1].IsNegativeVariableValue)
                                _right = -XValue;
                            else
                                _right = XValue;
                            break;
                        case Graphio_EquationVariable.Y:
                            if (parts[operatorIndex + 1].IsNegativeVariableValue)
                                _right = -YValue;
                            else
                                _right = YValue;
                            break;
                    }
                }
                else
                {
                    _right = parts[operatorIndex + 1].Value;
                }
            }
            switch (operation)
            {
                case Graphio_EquationOperation.Add:
                    _result = _left + _right;
                    break;
                case Graphio_EquationOperation.Sub:
                    _result = _left - _right;
                    break;
                case Graphio_EquationOperation.Mult:
                    _result = _left * _right;
                    break;
                case Graphio_EquationOperation.Div:
                    _result = _left / _right;
                    break;
                case Graphio_EquationOperation.Pow:
                    _result = (float)Math.Pow(_left, _right);
                    break;
                case Graphio_EquationOperation.Mod:
                    _result = _left % _right;
                    break;
                case Graphio_EquationOperation.RemoveParentheses:
                    parts.RemoveAt(operatorIndex);
                    parts.RemoveAt(secondaryIndex);
                    return;
                default: //TODO: throw error
                    break;
            }

            parts.RemoveRange(operatorIndex - 1, 3);
            parts.Insert(operatorIndex - 1, new Graphio_EquationPart(Graphio_EquationPartType.Number, _result));
        }

        private void PerformOperation(List<Graphio_EquationPart> parts, Graphio_EquationStep step)
        {
            float _result = 0;
            float _left = 0;
            float _right = 0;

            if(step.operation != Graphio_EquationOperation.RemoveParentheses)
            {
                // Check for variables
                if (parts[step.operatorIndex - 1].ContentType == Graphio_EquationPartType.Variable)
                {
                    switch (parts[step.operatorIndex - 1].Variable)
                    {
                        case Graphio_EquationVariable.X:
                            if (parts[step.operatorIndex - 1].IsNegativeVariableValue)
                                _left = -XValue;
                            else
                                _left = XValue;
                            break;
                        case Graphio_EquationVariable.Y:
                            if (parts[step.operatorIndex - 1].IsNegativeVariableValue)
                                _left = -YValue;
                            else
                                _left = YValue;
                            break;
                    }
                }
                else
                {
                    _left = parts[step.operatorIndex - 1].Value;
                }
                if (parts[step.operatorIndex + 1].ContentType == Graphio_EquationPartType.Variable)
                {
                    switch (parts[step.operatorIndex + 1].Variable)
                    {
                        case Graphio_EquationVariable.X:
                            if (parts[step.operatorIndex + 1].IsNegativeVariableValue)
                                _right = -XValue;
                            else
                                _right = XValue;
                            break;
                        case Graphio_EquationVariable.Y:
                            if (parts[step.operatorIndex + 1].IsNegativeVariableValue)
                                _right = -YValue;
                            else
                                _right = YValue;
                            break;
                    }
                }
                else
                {
                    _right = parts[step.operatorIndex + 1].Value;
                }
            }

            switch (step.operation)
            {
                case Graphio_EquationOperation.Add:
                    _result = _left + _right;
                    break;
                case Graphio_EquationOperation.Sub:
                    _result = _left - _right;
                    break;
                case Graphio_EquationOperation.Mult:
                    _result = _left * _right;
                    break;
                case Graphio_EquationOperation.Div:
                    _result = _left / _right;
                    break;
                case Graphio_EquationOperation.Pow:
                    _result = (float)Math.Pow(_left, _right);
                    break;
                case Graphio_EquationOperation.Mod:
                    _result = _left % _right;
                    break;
                case Graphio_EquationOperation.RemoveParentheses:
                    parts.RemoveAt(step.operatorIndex);
                    parts.RemoveAt(step.secondaryIndex);
                    return;
                default:
                    //TODO: throw error
                    break;
            }

            if (step.needRemoveSurroundingParentheses)
            {
                parts.RemoveRange(step.operatorIndex - 2, 5);
                parts.Insert(step.operatorIndex - 2, new Graphio_EquationPart(Graphio_EquationPartType.Number, _result));
            }
            else
            {
                parts.RemoveRange(step.operatorIndex - 1, 3);
                parts.Insert(step.operatorIndex - 1, new Graphio_EquationPart(Graphio_EquationPartType.Number, _result));
            }         
        }

        private void CheckForOperation(List<Graphio_EquationPart> parts, List<Graphio_EquationStep> steps, Graphio_EquationOperation operation, int indexOffset = 0, bool needRemoveParentheses = false)
        {
            for (int i = 0; i < parts.Count; i++)
            {
                if (parts[i].ContentType == Graphio_EquationPartType.Operation)
                {
                    if (parts[i].Operation == operation)
                    {
                        //if this is the last operation in this list of parts, then use the needRemoveParentheses parameter (so we don't try to prematurely remove them and accidentally remove other parts
                        if(parts.Count == 3)
                        {
                            steps.Add(new Graphio_EquationStep(operation, i + indexOffset, needRemoveSurroundingParentheses: needRemoveParentheses));
                        }
                        else
                        {
                            steps.Add(new Graphio_EquationStep(operation, i + indexOffset));
                        }

                        PerformOperation(parts, parts[i].Operation, i);
                    }
                }
            }
        }

        private List<Vector2> GetGraphValues(Graphio_Equation equation)
        {
            float _start = 0;
            float _last = 0;
            float _step = 0;
            bool autogen = true;
            List<Vector2> values = new List<Vector2>();

            // if we're solving for x, then check if we are either using a list of Y values or are autogenerating them
            if (equation.isXEqualsFormat)
            {
                if (AutoGenerateYLabels)
                {
                    _start = FirstY;
                    _last = LastY;
                    _step = StepY;
                }
                else
                {
                    autogen = false;
                }
                
            }//do the same if we're solving for y instead
            else
            {
                if (AutoGenerateXLabels)
                {
                    _start = FirstX;
                    _last = LastX;
                    _step = StepX;
                }
                else
                {
                    autogen = false;
                }
            }

            if (autogen)
            {
                for(float i = _start; i < _last; i += _step)
                {
                    if (equation.isXEqualsFormat)
                    {
                        YValue = i;
                        values.Add(new Vector2(Solve(equation), i));
                    }
                    else
                    {
                        XValue = i;
                        values.Add(new Vector2(i, Solve(equation)));
                    }
                }
            }

            return values;
        }

        private float Solve(Graphio_Equation equation)
        {
            List<Graphio_EquationPart> _tempParts = new List<Graphio_EquationPart>(equation.EquationParts);
            foreach(Graphio_EquationStep step in equation.Steps)
            {
                PerformOperation(_tempParts, step);
            }
            // for some reason first equation, following steps: 'x = 0 2 )'
            //check equation has actually be solved
            if (_tempParts.Count < 4)
            {
                if (_tempParts[2].ContentType == Graphio_EquationPartType.Number)
                {
                    return _tempParts[2].Value;
                }
                else return 0;
            }
            else return 0;
            //TODO: error handling
        }
    }
}