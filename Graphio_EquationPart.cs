﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphio
{
    internal struct Graphio_EquationPart
    {
        /// <summary>
        /// What type of character is at a specific point in the equation
        /// </summary>
        internal Graphio_EquationPartType ContentType;
        /// <summary>
        /// If ContentType is Number
        /// </summary>
        internal float Value;
        /// <summary>
        /// If the variable is actually a negative, e.g. -y or -x
        /// </summary>
        internal bool IsNegativeVariableValue;
        /// <summary>
        /// If ContentType is Variable
        /// </summary>
        internal Graphio_EquationVariable Variable;
        /// <summary>
        /// If ContentType is Operation
        /// </summary>
        internal Graphio_EquationOperation Operation;
        /// <summary>
        /// If ContentType is Parenthesis
        /// </summary>
        internal Graphio_ParenthesisType Parenthesis;

        public Graphio_EquationPart(Graphio_EquationPartType contentType, float value = 0, Graphio_EquationVariable variable = Graphio_EquationVariable.X, Graphio_EquationOperation operation = Graphio_EquationOperation.Add, Graphio_ParenthesisType parenthesis = Graphio_ParenthesisType.Close, bool isNegative = false)
        {
            ContentType = contentType;
            Value = value;
            Variable = variable;
            Operation = operation;
            Parenthesis = parenthesis;
            IsNegativeVariableValue = isNegative;
        }
    }
}
