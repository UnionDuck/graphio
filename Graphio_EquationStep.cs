﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphio
{
    internal struct Graphio_EquationStep
    {
        /// <summary>
        /// index in part array of the operator
        /// </summary>
        internal int operatorIndex;
        //optional int for storing secondary index for certain operations
        internal int secondaryIndex;
        internal Graphio_EquationOperation operation;
        internal bool needRemoveSurroundingParentheses;

        public Graphio_EquationStep(Graphio_EquationOperation operation, int index = 0, int secIndex = 0, bool needRemoveSurroundingParentheses = false)
        {
            this.operatorIndex = index;

            this.secondaryIndex = secIndex;

            this.operation = operation;

            this.needRemoveSurroundingParentheses = needRemoveSurroundingParentheses;
        }

        public void NeedRemoveParentheses()
        {
            needRemoveSurroundingParentheses = true;
        }
    }
}
