﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphio
{
    internal struct Graphio_Equation
    {
        internal List<Graphio_EquationPart> EquationParts;
        internal List<Graphio_EquationStep> Steps;
        internal string StringEquation;
        //Whether this is x=y or y=x
        internal bool isXEqualsFormat;

        public Graphio_Equation(List<Graphio_EquationPart> equationParts, List<Graphio_EquationStep> steps, string stringEquation, bool isXEqualsFormat = true)
        {
            EquationParts = equationParts;
            Steps = steps;
            StringEquation = stringEquation;
            this.isXEqualsFormat = isXEqualsFormat;
        }
    }
}
