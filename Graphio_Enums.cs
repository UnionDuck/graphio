﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphio
{
    public enum Graphio_GraphType
    {
        Line,
        Bar
    }

    public enum Graphio_GraphOrientation
    {
        Horizontal, 
        Vertical
    }

    internal enum Graphio_EquationOperation
    {
        Add,
        Sub,
        Mult,
        Div,
        Pow,
        Mod,
        RemoveParentheses
    }

    internal enum Graphio_EquationPartType
    {
        Number,
        Variable,
        Operation,
        Parenthesis,
        Equals
    }

    internal enum Graphio_EquationVariable
    {
        X,
        Y
    }

    internal enum Graphio_ParenthesisType
    {
        Open,
        Close
    }
}
